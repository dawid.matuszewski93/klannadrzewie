﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class InputController : MonoBehaviour
{
   static public InputController inputController;

   public event Action<MOUSE_KEY> MouseButtonClicked; 

   private void Awake()
   {
      inputController = this;
   }

   private void Update()
   {
      if (Input.GetMouseButtonDown(0))
      {
         MouseButtonClicked.Invoke(MOUSE_KEY.LEFT);
      }

      if (Input.GetMouseButtonDown(1))
      {
         MouseButtonClicked.Invoke(MOUSE_KEY.RIGHT);
      }
   }

   public enum MOUSE_KEY
   {
      LEFT = KeyCode.Mouse0,
      RIGHT = KeyCode.Mouse1
   }
}
