﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedCombatController : MonoBehaviour, ICombatAbilities
{
    public void Attack()
    {
        Debug.Log("Ranged Basic Attack");
    }

    public void Special()
    {
        Debug.Log("Ranged Special Attack");
    }
}
