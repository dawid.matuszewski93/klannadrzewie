﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCombatController : MonoBehaviour, ICombatAbilities
{
    public void Attack()
    {
        Debug.Log("Melee Combat Basic Attack");
    }

    public void Special()
    {
        Debug.Log("Melee Combat Special Attack");
    }
}
