﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PlayerMovementController : MonoBehaviour, IEntityMovement
{
    private Rigidbody _rigidbody;
    private IAxisProvider _axisProvider;

    private void Awake()
    {
        _axisProvider = GetComponent<IAxisProvider>();
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    public void Move()
    {
        Vector3 newVelocity = 500f * Time.deltaTime * new Vector3(_axisProvider.GetHorizontalAxis(true), 0, _axisProvider.GetVerticalAxis(true));
        _rigidbody.velocity = newVelocity;
        
    }
}
