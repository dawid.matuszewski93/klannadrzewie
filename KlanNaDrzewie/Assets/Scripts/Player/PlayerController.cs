﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private IEntityMovement _movementController;
    private ICombatAbilities _combatController;
    private InputController _inputController;

    private void Awake()
    {
        _movementController = GetComponent<IEntityMovement>();
        _combatController = GetComponent<ICombatAbilities>();
    }

    private void Start()
    {
        _inputController = InputController.inputController;
        _inputController.MouseButtonClicked += Attack;
    }

    void Update()
    {
        _movementController.Move();
    }

    void Attack(InputController.MOUSE_KEY key)
    {
        switch (key)
        {
            case InputController.MOUSE_KEY.LEFT:
                _combatController.Attack();
                break;
            case InputController.MOUSE_KEY.RIGHT:
                _combatController.Special();
                break;
        }
    }
}