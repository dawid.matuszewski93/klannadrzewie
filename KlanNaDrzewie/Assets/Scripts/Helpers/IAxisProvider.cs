﻿interface IAxisProvider
{
    float GetHorizontalAxis(bool isRaw = false);
    float GetVerticalAxis(bool isRaw = false);
}