﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAxisProvider : MonoBehaviour, IAxisProvider
{

    public float GetHorizontalAxis(bool isRaw)
    {
        if (isRaw)
        {
            return Input.GetAxisRaw("Horizontal");
        }

        return Input.GetAxis("Horizontal");
    }

    public float GetVerticalAxis(bool isRaw)
    {
        if (isRaw)
        {
            return Input.GetAxisRaw("Vertical");
        }

        return Input.GetAxis("Vertical");
    }
}
